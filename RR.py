# -*- coding: utf-8 -*-
"""
Created on Wed Mar  2 22:07:15 2019

@author: jordan
"""
from random import randint
# Define a process object
class Process(object):
    def __init__(self, ident, serviceT, arrivalT, enqueueT, startT, endT, initialWait, totalWait, turnAround, avgTurnAround, avgServiceT, remainT, activeC ):
        self.ident = ident
        self.serviceT = serviceT
        self.arrivalT = arrivalT
        self.enqueueT = enqueueT
        self.startT = startT
        self.endT = endT
        self.initialWait = initialWait
        self.totalWait = totalWait
        self.turnAround = turnAround
        self.avgTurnAround = avgTurnAround
        self.avgServiceT = avgServiceT
        self.remainT = remainT
        self.activeC = activeC

# Print out the processes (verbose form)
def printDetailedProcesses(procs):
    for proc in procs:
        print("ID: " + str(proc.ident))
        print("Service Time: " + str(proc.serviceT))
        print("Arrival Time: " + str(proc.arrivalT))
        print("Enqueue Time: " + str(proc.enqueueT))
        print("Start Time: " + str(proc.startT))
        print("End Time: " + str(proc.endT))
        print("Initial Wait Time: " + str(proc.initialWait))
        print("Total Wait Time: " + str(proc.totalWait))
        print("Turn Around Time: " + str(proc.turnAround))
        print("Average Turn Around Time: " + str(proc.avgTurnAround))
        print("Average Service Time: " + str(proc.avgServiceT))
        print("Remaining Service Time: " + str(proc.remainT))
        print("Active Count: " + str(proc.activeC))
        print("-----------------------------------------------")
        
# Print out a process 
def printProcess(proc):
    print(str(proc.ident) + "\t" + 
          str(proc.serviceT) + "\t" +
          str(proc.arrivalT) + "\t" +
          str(proc.enqueueT) + "\t" + 
          str(proc.startT) + "\t" +
          str(proc.endT) + "\t" +
          str(proc.initialWait) + "\t" +
          str(proc.totalWait) + "\t" +
          str(proc.turnAround) + "\t" +
          str(proc.avgTurnAround) + "\t" +
          str(proc.avgServiceT) + "\t" + 
          str(proc.remainT) + "\t" +
          str(proc.activeC))

# Print out all processes (short form)        
def printProcesses(procs):
    print("ID\tSvcT\tArrT\tenqueueT\tStartT\tEndT\tIniWaitT\tTotWaitT\tTAT\tavgTAT\tavgSvcT\tremainT\tactiveC")
    for proc in procs:
        printProcess(proc)

# Run the simulation given a list of processes, the quantum, and context switch time
def runScheduler(procs,quantum,cs):
    t = 0  # start at cpu clock time 0
    csT = 0  # initial context switch timer is set to zero
    qT = 0  # initial quantum timer is set to zero
    queue = []  # start with an empty processing queue
    currA = -1  # the index of the active process in the queue
    completed = []  # list of completed processes
    
    # Keep ticking time forward by 1 until both procs and queue are empty
    while len(procs) > 0 or len(queue) > 0:
        # dispatch any new processes to queue if it's time
        e = 0  # keep track of enqueued processes
        for i in range(len(procs)):
            if procs[i-e].arrivalT == t:  # use offset index by e
                # Adding a process to the queue here
                procs[i-e].enqueueT = t  # set the enqueue time
                queue.append(procs.pop(i-e))  # append to the queue
                e += 1  # add one to the enqueued count for index offsetting
                if len(queue) == 1: # if the queue was just empty
                    qT = quantum # reset the quantum timer (no need for a cs!)
                    currA = 0 # set the current active proc index
        # if we are not context switching, lets process
        if csT == 0:
            # ensure there is an active proc in the queue 
            if len(queue) > 0 and currA > -1:
                if queue[currA].remainT == queue[currA].serviceT:
                    queue[currA].startT = t  # mark the start time
                    queue[currA].initialWait = queue[currA].startT - queue[currA].enqueueT
                queue[currA].remainT -= 1  # decrement active proc remaining time
                # if quantum time is zero or current process is finished
                if qT == 0 or queue[currA].remainT == 0:
                    csT = cs  # reset context switching timer
                    qT = quantum  # reset quantum timer (even if process finished!)
                    queue[currA].activeC += 1 # increment the active process counter
                    if queue[currA].remainT == 0:  # if no time remaining for proc
                        queue[currA].endT = t+1  # mark the end time (add one because it is still finishing during this current time step)
                        queue[currA].totalWait = queue[currA].endT - queue[currA].arrivalT - queue[currA].serviceT
                        queue[currA].turnAround = queue[currA].endT - queue[currA].arrivalT
                        queue[currA].avgTurnAround = round(queue[currA].turnAround / queue[currA].activeC, 2)
                        queue[currA].avgServiceT = round(queue[currA].serviceT / queue[currA].activeC, 2)
                        
                        completed.append(queue.pop(currA))  # remove active proc from queue and add to completed list
                        currA -= 1 # decrement because we just popped
                    if currA < len(queue)-1:  # are we at least 1 from the end of the queue
                        currA += 1  # increment to next in queue
                    else:  # we are at the end of the queue
                        if len(queue) > 0:  # if the queue is not empty
                            currA = 0   # go back to the start of queue
                        else:
                            currA = -1  # we have an empty queue, set to invalid for now

                if qT > 0:
                    qT -= 1   # decrement quantum timer
        else:
            csT -= 1  # decrement context switch timer
        t += 1 # advance the cpu clock
    return completed


#################### Main Program Begins Here ##############################

# Initial Conditions (all three lists must be of the same length)
#ids = [1,2,3,4,5]
ids = list(range(1,100))
#serviceTimes = [75,40,25,20,45]

#########Begin generation of Service Times #######################
# generate 100 service times between 2 and 5 
#  serviceTime = [randint(2, 5) for k in range(0, 100)] 
def genServiceTime():
  serviceTimes = [randint(2,5) for k in range(0, 100)]
  #print(serviceTimes)
  return serviceTimes

#arrivalTimes = [0,10,10,80,85]
#serviceTimes = [] # start with an empty list 
serviceTimes = genServiceTime()
############# Begin generation of Arrival Times ###################
def genArriveT():
    arrivalTimes = [0] 
    for i in range(1, 100):
        iat = randint(4,8)
        lastArrivalTime = arrivalTimes[len(arrivalTimes)-1]
        arrivalTimes.append(lastArrivalTime + iat)
    return arrivalTimes
    
arrivalTimes = genArriveT()
################################################################
def getIATs(arrivalTimes):
    IATs = []
    lastAT = arrivalTimes[0]
    for i in range(1,len(arrivalTimes)-1):
        IATs.append(arrivalTimes[i]-lastAT)
        lastAT = arrivalTimes[i]
    return IATs
###############################################################
#IATs = getIATs(arrivalTimes)
    
quantum = 2
cs = 0

# List to store processes
procs = []

# Popluate the process list (remainT = serviceT)
for i in range(len(ids)):
    procs.append(Process(ids[i], serviceTimes[i], arrivalTimes[i], 
                -1,-1,-1,-1,-1,-1,-1,-1, serviceTimes[i],0))

# Input quantum and cs from the user:
quantum = int(input("Enter quantum: "))
cs = int(input("Enter context switch time: "))

printDetailedProcesses(procs[0:1])

# Print Initial Process List
print("---------------------Initial Process List------------------------------")
printProcesses(procs)

# Run the Simulator
completed = runScheduler(procs,quantum,cs)

# Print results
print("---------------------Completed Process List------------------------------")
printProcesses(completed)


# Post Processing summary results:
print("---------------------Summary Statistics------------------------------")

# Inter Arrival Time
IATs = []
arrivalTimes.sort()
for idx in range(len(arrivalTimes)-1):
    IATs.append(arrivalTimes[idx+1]-arrivalTimes[idx])
interArrivalTime = sum(IATs)/float(len(IATs))
print("Average Inter-Arrival Time between all " + str(len(completed)) + " processes is: " + str(interArrivalTime))
# Average Turn Around Time
totalTurnAround = 0
for i in range(len(completed)):
    totalTurnAround += completed[i].turnAround
print("Average TurnAround Time for all " + str(len(completed)) + " processes is: " + str(totalTurnAround/len(completed)))
print("Average Service Time for all " + str(len(completed)) + " processes is: " + str(sum(serviceTimes)/float(len(serviceTimes))))


#printDetailedProcesses(procs)
#printProcesses(procs)
